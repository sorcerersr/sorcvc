# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#


import os
from globals import Globals
from sorcvc_src.project import Project
from ui.filesystem import FileSystem
from log import Log


class GuiController(object):
    """
    Backend for the UI to perform non UI logic.
    """

    __shared_state = {}  # borg pattern

    app_name = Globals.app_name
    app_ver = Globals.app_ver
    app_desc = Globals.app_desc

    instance = None

    def __init__(self):
        self.__dict__ = self.__shared_state # borg pattern
        if not self.instance:
            self.instance = True
            self.window = None

    def set_window(self, window):
        """
        Let the gui controller know with which window it works together
        """
        self.window = window

    def set_working_path(self, path):
        """
        Sets the current working path. May be a local directory or a remote url
        """
        # first get the selected project
        project = self.window.projectlist.get_selected_project()
        # based on the type of the project set the working dir
        # or the working url
        if project.typ == Project.WORKING_COPY:
            self.set_local_working_dir(path)
        else:
            self.set_remote_working_url(path)

    def set_local_working_dir(self, path):
        fs = self.window.workbench.filesystemview
        fs.clear()

        fs.set_path(path)
        content = os.listdir(path)
        for entry in content:
            if os.path.isdir(os.path.join(path, entry)):
                entry_type = FileSystem.TYPE_FOLDER
            else:
                entry_type = FileSystem.TYPE_FILE
            fs.add_entry(entry_type, entry)

        self.window.show_workbench()

    def set_remote_working_url(self, url):
        self.window.show_repobrowser()
