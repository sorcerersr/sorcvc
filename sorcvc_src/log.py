# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#

from logging.handlers import RotatingFileHandler
from sorcvc_src.globals import Globals
import logging


class Log(object):
    """
    This class handles the initialization of an application logger.
    """

    log = None

    def __init__(self, level=logging.INFO):
        """
        Constructor

        @parameter level - the logging level to use
        """
        log_file = Globals.config_dir + '/sorcvc.log'
        Log.log = logging.getLogger('SorcVC')
        #initialize logging
        #rotating filehandler using a 2MB logfile and keep 5 backups
        filehandler = RotatingFileHandler(log_file,
                                          maxBytes=2097152,
                                          backupCount=5)
        myformat = '%(asctime)s\t%(levelname)s\t%(message)s'
        formatter = logging.Formatter(myformat)
        logging.basicConfig(format=myformat)
        filehandler.setFormatter(formatter)
        Log.log.addHandler(filehandler)
        Log.log.setLevel(level)