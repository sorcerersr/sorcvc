#!/usr/bin/env python
# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#


import getopt

import logging
import os
import sys

from gi.repository import Gtk

from sorcvc_src.globals import Globals
from sorcvc_src.log import Log
from sorcvc_src.project import Project
from sorcvc_src.ui.window import Window
from filemanager import FileManager
from guicontroller import GuiController


# noinspection PyBroadException
def try_gtk_imports():
    # gtk import stuff
    try:
        import pygtk

        pygtk.require("2.0")
    except:
        pass
    try:
        import gtk
    except:
        print('Import GTK libraries failed!')
        sys.exit(1)


# noinspection PyBroadException
def determine_path():
    """Borrowed from wxglade.py"""
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))
    except:
        print("I'm sorry, but something is wrong.")
        print("There is no __file__ variable. Please contact the author.")
        sys.exit()


def usage():
    """
    Prints out usage information
    Commandline parameters
    """
    print('Available commandline options:')
    print('\t-h or --help\tthis helptext')
    print('\t-d or --debug\tactivating debug output')


def start():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hd", ["help", "debug"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    log_level = logging.INFO

    for o, a in opts:
        if o in ("-d", "--debug"):
            log_level = logging.DEBUG
        elif o in ("-h", "--help"):
            usage()
            sys.exit(1)
        else:
            assert False, "unhandled option"

    try_gtk_imports()

    gobject.threads_init()  # enable threads for gtk

    # init global definitions:
    Globals.initialize(determine_path())

    # init logging
    Log(log_level)
    Log.log.info('Starting....')
    if sys.platform == 'win32':
        # apply gtk style for win32 (wimp theme)
        gtkrc_path = os.path.normpath(os.path.join(Globals.resource_dir,
                                                   'win_gtk/gtkrc'))
        gtk.rc_parse(gtkrc_path)

    window = Window()  # create the sorcvc window
    gui_controller = GuiController()  # initialize the gui controller
    gui_controller.set_window(window)

    gtk.main()  # run the gtk main loop

    # on exit save the window size information
    Log.log.info('Exiting....')
    fm = FileManager()
    Log.log.debug('Saving window size information')
    fm.save_window_sizing()


if __name__ == "__main__":
    start()
