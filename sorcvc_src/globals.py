# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#

import os
import sys


class Globals(object):
    """
    Defines some global variables.

    This are the path to the config folder and the location the py scripts are
    located.
    """

    app_name = 'SorcVC'
    app_ver = '0.1'
    app_desc = 'a UI frontend to subversion written in python using GTK'

    app_root = None
    package_dir = None
    resource_dir = None
    config_dir = None

    icon_dir = None

    @staticmethod
    def initialize(packagepath):
        """
        static init method to initialize static variables
        """
        Globals.package_dir = packagepath
        Globals.app_root = os.path.normpath(os.path.join(Globals.package_dir,
                                                         '../'))
        Globals.resource_dir = os.path.join(Globals.package_dir, 'resource')

        if not os.path.exists(Globals.resource_dir):
            #this should be the case when building windows distribution using
            #py2exe where the resource directory is not located inside the
            #package structure
            #start with the packagedir minus two directories (this will be
            #the dist dir of py2exe)
            Globals.resource_dir = os.path.join(Globals.package_dir,
                                                '../../resource')
            #if this also does not exist break, program will not work and
            #throw errors when the icons could not be loaded
            if not os.path.exists(Globals.resource_dir):
                print('Resource directory not found ! Exiting ...')
                sys.exit(2)

        Globals.config_dir = Globals.__determine_config_dir()

        # resource dirs
        Globals.icon_dir = os.path.join(Globals.resource_dir, 'icons')

        print(Globals.package_dir + '   - (package_dir)')
        print(Globals.app_root + '   - (app_root)')
        print(Globals.resource_dir + '   - (resource_dir)')
        print(Globals.config_dir + '    - (config_dir)')

    @staticmethod
    def __determine_config_dir():
        config_dir = os.path.expanduser('~/.sorcvc/')
        if not os.path.exists(config_dir):
            try:
                os.makedirs(config_dir)
            except OSError:
                #fallback to app root
                config_dir = os.path.join(Globals.package_dir, '../')
                pass
        return config_dir
