# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#


import pickle

from globals import Globals
from project import Project


class WindowSizing:
    """
    Container class used to hold window sizing information.
    Used to persist and load this information to and from a file.
    """
    def __init__(self):
        """
        Constructor
        """
        #defaults:
        self.height = 768
        self.width = 1024
        self.maximized = False
        self.x = None
        self.y = None


class FileManager(object):
    """
    This class handles all internal file accesses of SorcVC like
    reading/writing of configuration
    """
    __shared_state = {}  # borg pattern

    instance = None

    configuration = None
    window_sizing = None

    #filenames
    projects_file = 'projects.cfg'
    window_size_file = 'window_size.pkl'

    def __init__(self):
        """
        Constructor
        """
        self.__dict__ = self.__shared_state  # borg pattern

        if not self.instance:
            self.instance = True

            # extend filenames with path to the configurationfolder
            self.window_size_file = Globals.config_dir+FileManager.\
                window_size_file
            self.projects_file = Globals.config_dir+FileManager.projects_file

            # the stores
            self.projects = None

    def get_window_sizing(self):
        """
        Loads the window size settings from the file or returns a already
        loaded instance if available
        """
        if self.window_sizing:
            return self.window_sizing

        try:
            pkl_file = open(self.window_size_file, "rb")
        except IOError:
            self.window_sizing = WindowSizing()
            return self.window_sizing

        self.window_sizing = pickle.load(pkl_file)

        pkl_file.close()
        return self.window_sizing

    def save_window_sizing(self):
        """
        Saves the window sizing instance in a file
        """
        pkl_file = open(self.window_size_file, "wb")

        pickle.dump(self.window_sizing, pkl_file, pickle.HIGHEST_PROTOCOL)

        pkl_file.close()

    def save_projects(self):
        """
        Performs the writing to the projects file.
        """
        fobj = open(self.projects_file, "w")
        for key in self.projects:
            project = self.projects[key]
            # format: name,type,location,order
            fobj.write(project.name + ',' + str(project.typ) + ','
                       + project.location + ',' + str(project.order) + '\n')

        fobj.close()

    def get_projects(self):
        """
        Get the projects. If already loaded from file, return the instance from
        memory, otherwise load from file.

        @return dict of projects objects (key = name)
        """
        if self.projects:
            return self.projects

        self.projects = {}

        try:
            fobj = open(self.projects_file, "r")
        except IOError:
            #the file does not exist! just return the empty dict created earlier
            return self.projects

        for line in fobj:
            line = line.strip()
            project_info = line.split(',')  # file is a csv
            #format: name,type,location,order
            project = Project(project_info[0],
                              int(project_info[1]),
                              project_info[2],
                              int(project_info[3]))

            # store the project into the dict using the name as key
            self.projects[project.name] = project
        fobj.close()

        return self.projects