# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#

import gtk
from ..guicontroller import GuiController
from ..filemanager import FileManager
from ..globals import Globals
from ..log import Log
from sorcvc_src.ui.repobrowser import RepoBrowser
from sorcvc_src.ui.statusbar import StatusBar
from workbench import WorkBench
from sorcvc_src.ui.projectlist import ProjectList


class Window(gtk.Window):
    """
    This class is the entry point of the UI part of SorcVC.
    It creates the GTK Window.
    """
    #override configure event to react in window resize
    __gsignals__ = {"configure-event": "override"}

    instance = None

    def __init__(self):
        """
        Constructor. Creating and initializing the main window of SorcVC.
        """
        gtk.Window.__init__(self)
        Window.instance = self

        # connect destroy event to end gtk main loop
        self.connect('destroy', gtk.main_quit)

        #window creation and basic window settings (size etc.)
        fm = FileManager()
        window_sizing = fm.get_window_sizing()

        if window_sizing.maximized:
            self.maximize()
        else:
            self.unmaximize()
        self.set_default_size(window_sizing.width, window_sizing.height)
        if None == window_sizing.x and None == window_sizing.y:
            self.set_position(gtk.WIN_POS_CENTER)
        else:
            self.move(window_sizing.x, window_sizing.y)

        # set title and logo
        gc = GuiController()
        self.set_title(gc.app_name + ' v.' + gc.app_ver + ' - ' + gc.app_desc)
        self.set_icon_from_file(Globals.icon_dir + '/logo.jpg')

        #add the content

        # add a VBox that will contain all the other widgets
        mainbox = gtk.VBox()
        self.add(mainbox)

        self.hpaned = gtk.HPaned()
        mainbox.pack_start(self.hpaned)

        self.projectlist = ProjectList()
        self.hpaned.add1(self.projectlist)

        self.workbench = WorkBench()
        self.repobrowser = RepoBrowser()
        self.welcome = gtk.Label('Welcome to SorcVC')

        self.current_widget = self.welcome
        self.hpaned.add2(self.welcome)


        statusbar = StatusBar()
        mainbox.pack_start(statusbar, False, False)

        #connect window-state-event to handle maximize/de-maximize
        self.connect('window-state-event', self.on_window_state_changed)

        # finally display the window
        self.show_all()

        #self.get_window().set_decorations(0)


    def do_configure_event(self, event):
        """
        overrides the default do_configure_event method
        handles window move and resize and stores this information
        """
        fm = FileManager()
        window_sizing = fm.get_window_sizing()
        window_sizing.width = event.width
        window_sizing.height = event.height
        window_sizing.x = event.x
        window_sizing.y = event.y

        # Log.log.debug('Window: size = ' + str(window_sizing.width) + 'x' +
        #               str(window_sizing.height) + '  position = ' +
        #               str(window_sizing.x) + ', ' + str(window_sizing.y))

        gtk.Window.do_configure_event(self, event)

    def on_window_state_changed(self, window, event):
        """
        Callback of the def "window-state-event"
        Needed to handle maximize/demaximize of the window
        """
        fm = FileManager()
        window_sizing = fm.get_window_sizing()
        if event.new_window_state & gtk.gdk.WINDOW_STATE_MAXIMIZED:
            Log.log.debug('SorcVC Window maximized')
            window_sizing.maximized = True
        else:
            Log.log.debug('SorcVC Window not maximized')
            window_sizing.maximized = False

    def show_workbench(self):
        self.hpaned.remove(self.current_widget)
        self.hpaned.add2(self.workbench)
        self.current_widget = self.workbench
        self.show_all()

    def show_welcomemessage(self):
        self.hpaned.remove(self.current_widget)
        self.hpaned.add2(self.welcome)
        self.current_widget = self.welcome
        self.welcome.show()
        self.show_all()

    def show_repobrowser(self):
        self.hpaned.remove(self.current_widget)
        self.hpaned.add2(self.repobrowser)
        self.current_widget = self.repobrowser
        self.repobrowser.show()
        self.show_all()
