# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#
import gtk
import os
from ..globals import Globals


class FileSystem(gtk.VBox):

    TYPE_FOLDER = 0
    TYPE_FILE = 1

    def __init__(self):
        gtk.VBox.__init__(self)

        # img, file/dirname, , type, combination of type and name for sorting
        self.liststore = gtk.ListStore(gtk.gdk.Pixbuf, str, int, str)

        self.liststore.set_sort_column_id(3, gtk.SORT_ASCENDING)

        sw = gtk.ScrolledWindow()
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.pack_start(sw)

        filesystemview = gtk.TreeView(model=self.liststore)
        filesystemview.set_headers_clickable(False)

        cell0 = gtk.CellRendererPixbuf()

        column_type = gtk.TreeViewColumn('')
        filesystemview.append_column(column_type)
        column_type.pack_start(cell0)
        column_type.add_attribute(cell0, 'pixbuf', 0)

        cell1 = gtk.CellRendererText()

        column_entry = gtk.TreeViewColumn('Entry')
        column_entry.pack_start(cell1)
        filesystemview.append_column(column_entry)
        column_entry.add_attribute(cell1, 'text', 1)

        sw.add(filesystemview)

        folder_image = gtk.Image()
        folder_image.set_from_file(Globals.icon_dir + '/folder_small.png')
        self.folder_pixbuf = folder_image.get_pixbuf()

        file_image = gtk.Image()
        file_image.set_from_file(Globals.icon_dir + '/file.png')
        self.file_pixbuf = file_image.get_pixbuf()

        filesystemview.connect('row-activated', self.on_row_double_click)

        self.path = None

    def clear(self):
        """
        Completely clears the filesystem display.
        """
        self.liststore.clear()

    def add_entry(self, entry_type, entry_name):
        """
        Adds an entry to the filesystem view
        """
        if entry_type == FileSystem.TYPE_FOLDER:
            pixbuf = self.folder_pixbuf
        else:
            pixbuf = self.file_pixbuf

        self.liststore.append([pixbuf,
                               entry_name,
                               entry_type,
                               str(entry_type) + entry_name])

    def set_path(self, path):
        """
        sets the path the current view is displaying
        """
        self.path = path

    def on_row_double_click(self, treeview, path, view_column):
        """
        Callback for the row-activate signal (double-click, enter...)
        """
        from ..guicontroller import GuiController
        row = self.liststore[path]
        name = row[1]
        type = row[2]
        if type == FileSystem.TYPE_FILE:
            return  # it is a file, so do nothing

        # clicked row is a folder
        # moving to it, in order to display its content
        gc = GuiController()
        gc.set_working_path(os.path.join(self.path, name))
