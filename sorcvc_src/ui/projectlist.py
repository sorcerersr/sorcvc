# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#
import gtk
from ..filemanager import FileManager
from ..globals import Globals
from ..guicontroller import GuiController
from ..project import Project
from sorcvc_src.ui.globalbuttons import GlobalButtonRow


class ProjectList(gtk.VBox):
    """
    Displays a list of projects
    Includes a add new and remove functionality
    """

    def __init__(self):
        """
        Constructor
        """
        gtk.VBox.__init__(self)
        self.buttonbox = gtk.HBox()
        self.pack_start(self.buttonbox, expand=False)

        sw = gtk.ScrolledWindow()
        sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.pack_start(sw)

        # creating the liststore, only two of the values are actualy displayed,
        # the rest is for internal use.
        # 0. Pixbuf - a image showing a folder or a server icon
        # 1. str - the label that is displayed
        # 2. str - name - the project name (not displayed)
        # 3. str - location (not displayed)
        # 4. int - order (not displayed, used for sorting)
        # 5. int - type (not displayed, working copy or repository)
        self.liststore = gtk.ListStore(gtk.gdk.Pixbuf, str, str, str, int, int)
        self.liststore.set_sort_column_id(4, gtk.SORT_ASCENDING)

        self.projectlistview = gtk.TreeView(model=self.liststore)
        self.projectlistview.set_headers_clickable(False)

        cell0 = gtk.CellRendererPixbuf()

        column_type = gtk.TreeViewColumn('')
        self.projectlistview.append_column(column_type)
        column_type.pack_start(cell0)
        column_type.add_attribute(cell0, 'pixbuf', 0)


        cell1 = gtk.CellRendererText()
        column_name = gtk.TreeViewColumn('Projects', cell1, markup=1)
        self.projectlistview.append_column(column_name)

        sw.add(self.projectlistview)

        fm = FileManager()
        projects = fm.get_projects()

        folder_image = gtk.Image()
        folder_image.set_from_file(Globals.icon_dir+'/folder.png')
        folder_pixbuf = folder_image.get_pixbuf()

        repo_image = gtk.Image()
        repo_image.set_from_file(Globals.icon_dir+'/network-server.png')
        repo_pixbuf = repo_image.get_pixbuf()

        for key in projects:
            project = projects[key]
            if project.typ == Project.WORKING_COPY:
                pixbuf = folder_pixbuf
            else:
                pixbuf = repo_pixbuf

            label = '<span><b>' + project.name + '</b></span>\n'
            label += '<span><small>' + project.location + '</small></span>'
            self.liststore.append([pixbuf,
                                   label,
                                   project.name,
                                   project.location,
                                   project.order,
                                   project.typ])

        lower_button_row = GlobalButtonRow()
        self.pack_start(lower_button_row, False, False)

        self.show()

        selection = self.projectlistview.get_selection()
        selection.connect('changed', self.onSelectionChanged)


    def onSelectionChanged(self, selection):
        """
        Callback handling the selection of a row in the projectlist.
        Triggers updating the content area of the parent tab.
        """

        model, paths = selection.get_selected_rows()
        if paths:
            row = model[paths[0][0]]
            # get the location and the type
            location = row[3]
            prj_type = row[5]

            guicontroller = GuiController()
            if prj_type == Project.WORKING_COPY:
                guicontroller.set_local_working_dir(location)
            else:
                guicontroller.set_remote_working_url(location)

    def get_selected_project(self):
        """
        returns the currently selected project
        """
        selection = self.projectlistview.get_selection()
        model, paths = selection.get_selected_rows()
        if paths:
            row = model[paths[0][0]]
            # get the project name
            name = row[2]

            fm = FileManager()
            projects = fm.get_projects()
            project = projects[name]
            return project