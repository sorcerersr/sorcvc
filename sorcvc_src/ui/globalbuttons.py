# coding=utf-8
#
#    Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
#
#    This file is part of sorcvc.
#
#    sorcvc is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sorcvc is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von sorcvc.
#
#    sorcvc ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
#    veröffentlichten Version, weiterverbreiten und/oder modifizieren.
#
#    sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
#
import gtk
from aboutdialog import AboutDialog


class GlobalButtonRow(gtk.HBox):

    def __init__(self):
        gtk.HBox.__init__(self)

        settings_btn = gtk.Button()
        settingsimage = gtk.Image()
        settingsimage.set_from_stock(gtk.STOCK_PREFERENCES,
                                     gtk.ICON_SIZE_SMALL_TOOLBAR)
        settings_btn.set_image(settingsimage)

        self.pack_start(settings_btn, False, False, 2)

        about_btn = gtk.Button()
        aboutimage = gtk.Image()
        aboutimage.set_from_stock(gtk.STOCK_ABOUT,
                                  gtk.ICON_SIZE_SMALL_TOOLBAR)
        about_btn.set_image(aboutimage)
        about_btn.connect("clicked", self.on_about_clicked)

        self.pack_start(about_btn, False, False, 2)

    def on_about_clicked(self, widget):
        """
        Callback of the about button
        """
        about_dialog = AboutDialog()
        about_dialog.run()
        about_dialog.hide()