# coding=utf-8
#
   # Copyright (C) 2014  Sorcerer (sorcerersr@mailbox.org)
   #
   # This file is part of sorcvc.
   #
   # sorcvc is free software: you can redistribute it and/or modify
   # it under the terms of the GNU General Public License as published by
   # the Free Software Foundation, either version 3 of the License, or
   # (at your option) any later version.
   #
   # sorcvc is distributed in the hope that it will be useful,
   # but WITHOUT ANY WARRANTY; without even the implied warranty of
   # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   # GNU General Public License for more details.
   #
   # You should have received a copy of the GNU General Public License
   # along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.
   #
   # Diese Datei ist Teil von sorcvc.
   #
   # sorcvc ist Freie Software: Sie können es unter den Bedingungen
   # der GNU General Public License, wie von der Free Software Foundation,
   # Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
   # veröffentlichten Version, weiterverbreiten und/oder modifizieren.
   #
   # sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber
   # OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
   # Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
   # Siehe die GNU General Public License für weitere Details.
   #
   # Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
   # Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
   #
from ..globals import Globals
from ..guicontroller import GuiController
import gtk


class AboutDialog(gtk.AboutDialog):
    """
    The About dialog of SorcVC
    """

    gplstring1 = '\n\
        sorcvc is free software: you can redistribute it and/or modify\n\
        it under the terms of the GNU General Public License as published\n\
        by the Free Software Foundation, either version 3 of the License,\n\
        or (at your option) any later version.\n'
    gplstring2 = '\n\
        sorcvc is distributed in the hope that it will be useful,\n\
        but WITHOUT ANY WARRANTY; without even the implied warranty\n\
        of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\
        See the GNU General Public License for more details.\n'
    gplstring3 = '\n\
        You should have received a copy of the GNU General Public License\n\
        along with sorcvc.  If not, see <http://www.gnu.org/licenses/>.\n\n'
    gplstring4 = '\n\
        sorcvc ist Freie Software: Sie können es unter den Bedingungen\n\
        der GNU General Public License, wie von der Free Software\n\
        Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder\n\
        späteren veröffentlichten Version, weiterverbreiten und/oder\n\
        modifizieren.\n'
    gplstring5 = '\n\
        sorcvc wird in der Hoffnung, dass es nützlich sein wird, aber\n\
        OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite\n\
        Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN\n\
        BESTIMMTEN ZWECK. Siehe die GNU General Public License für\n\
        weitere Details.\n'
    gplstring6 = '\n\
        Sie sollten eine Kopie der GNU General Public License zusammen mit\n\
        diesem Programm erhalten haben. Wenn nicht,\n\
        siehe  <http://www.gnu.org/licenses/>.\n'

    def __init__(self):
        """
        Constructor
        """
        gtk.AboutDialog.__init__(self)
        
        gc = GuiController()
        self.set_name(gc.app_name)
        self.set_program_name(gc.app_name)
        self.set_version(gc.app_ver)
        self.set_comments(gc.app_desc)
        self.set_website('n/a')
        authors = ['Sorcerer (sorcerersr@mailbox.org)']
        self.set_authors(authors)
        self.set_copyright('(c) 2014')
        self.set_license(self.gplstring1 +
                         self.gplstring2 +
                         self.gplstring3 +
                         self.gplstring4 +
                         self.gplstring5 +
                         self.gplstring6)
        
        logo_image = gtk.Image()
        logo_image.set_from_file(Globals.icon_dir+'/logo.jpg')
        self.set_logo(logo_image.get_pixbuf())
